# How to run the project
`npm run start-client` for normal.
`npm run serve` for server side rendering

# Screenshot
![Life screenshot](public/life.gif)