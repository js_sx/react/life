import React from "react";
import bg from "../resources/background.jpg"

const PlayGround = ({
    children,
    height,
    width,
}) => {
    return <div style={{
        overflow: 'hidden',
        position: 'relative',
        background: `url(${bg})`,
        width: width + 'px',
        height: height + 'px',
    }}>
        {children}
    </div>
}

export default PlayGround;