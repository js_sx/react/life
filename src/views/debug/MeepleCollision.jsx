import config from "../../conf/config";

const MeepleCollision = ({ collisionInfo }) => {
    return (<>
        <div style={{
            position: 'absolute',
            left: collisionInfo.originTile.x * config.meeple.size,
            top: collisionInfo.originTile.y * config.meeple.size,
            width: config.meeple.size + 'px',
            height: config.meeple.size + 'px',
            background: collisionInfo.isMoving ? 'cyan' : 'lightPink'
        }} />
        <div style={{
            position: 'absolute',
            left: collisionInfo.destinationTile.x * config.meeple.size,
            top: collisionInfo.destinationTile.y * config.meeple.size,
            width: config.meeple.size + 'px',
            height: config.meeple.size + 'px',
            border: '1px solid red'
        }} />
    </>);
}

export default MeepleCollision;