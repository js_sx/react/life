import React from "react";
import config from "../conf/config";
import spriteUtils from "../utils/spriteUtils";
import sprites1 from "../resources/sprites_1.png"
import sprites2 from "../resources/sprites_2.png"
const spriteImages = [null, sprites1, sprites2];

const Meeple = ({
    position,
    orientation,
    characterStep,
    characterType,
}) => {
    const spriteConf = spriteUtils.sprites[characterType];
    const size = config.meeple.size;
    const characterOrientation = ['S', 'W', 'E', 'N'].findIndex((o => o === orientation));
    const xSprite = spriteConf.x + (size * characterStep);
    const ySprite = spriteConf.y + (size * characterOrientation);
    return (
        <div style={{
            position: 'absolute',
            left: position.x,
            top: position.y,
            background: `url(${spriteImages[spriteConf.image]}) ${-xSprite}px ${-ySprite}px`,
            width: size + 'px',
            height: size + 'px',
            color: 'white'
        }} />);

}

export default Meeple;