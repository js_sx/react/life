import React from "react";
import Meeple from "./Meeple";
import meepleFactory from "../factories/meepleFactory";
import spriteUtils from "../utils/spriteUtils";

const meepleConf = { position: { x: '40px', y: '18px' } };
const orientations = ['S', 'E', 'N', 'W'];
const spriteTypeCount = spriteUtils.sprites.length;

const MeepleBuilder = ({ onCreate }) => {
    const [meeple, setMeeple] = React.useState(meepleFactory.getNewMeeple(meepleConf))

    const turnLeft = () => setMeeple({ ...meeple, orientation: orientations[(orientations.findIndex(o => o === meeple.orientation) + 3) % 4] });
    const turnRight = () => setMeeple({ ...meeple, orientation: orientations[(orientations.findIndex(o => o === meeple.orientation) + 1) % 4] });
    const nextType = () => setMeeple({ ...meeple, characterType: (meeple.characterType + 1) % spriteTypeCount });
    const prevType = () => setMeeple({ ...meeple, characterType: (meeple.characterType + spriteTypeCount - 1) % spriteTypeCount });
    const onOk = () => onCreate && onCreate({...meeple, position: {x:0, y:0}})

    return (
        <div style={{
            position: 'relative',
            border: '1px solid black',
            borderRadius: '5px',
            width: '112px',
            height: '98px',
            margin: '2px'
        }}>
            <button onClick={prevType}
                style={{
                    width: '24px',
                    position: 'absolute',
                    top: '8px',
                    left: '8px'
                }}>◄</button>
            <button onClick={turnLeft}
                style={{
                    width: '24px',
                    position: 'absolute',
                    top: '40px',
                    left: '8px'
                }}>⤾</button>

            <Meeple {...meeple} />
            <button onClick={nextType}
                style={{
                    width: '24px',
                    position: 'absolute',
                    top: '8px',
                    left: '80px'
                }}>►</button>
            <button onClick={turnRight}
                style={{
                    width: '24px',
                    position: 'absolute',
                    top: '40px',
                    left: '80px'
                }}>⤿</button>

            <button onClick={onOk}
                style={{
                    width: '32px',
                    position: 'absolute',
                    top: '68px',
                    left: '40px'
                }}>Ok</button>
        </div>
    );
}

export default MeepleBuilder;