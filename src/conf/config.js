const config = {
    debug: false,
    meeple: {
        size: 32
    },
    playground: {
        width: 32 * 30,
        height: 32 * 20
    },
    movement: {
        tickEvery: 60,
        maxStayTicks: 64
    }
};

export default config;