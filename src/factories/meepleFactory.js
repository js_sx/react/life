const meepleFactory = {
    getNewMeeple: (conf) => (
        {
            id: -1,
            characterType: 0,
            characterStep: 1,
            orientation: 'S',
            actionCount: 0,
            action: null,
            position: { x: 0, y: 0 },
            collisionInfo: {
                isMoving: false,
                inTransition: false,
                originTile: { x: 0, y: 0 },
                destinationTile: { x: 0, y: 0 },
            },
            ...conf
        }
    ),

}

export default meepleFactory;