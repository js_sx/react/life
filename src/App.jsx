import React from "react";
import Meeple from './views/Meeple'
import PlayGround from './views/Playground'
import meepleUtils from './utils/meepleUtils'
import config from "./conf/config";
import meepleFactory from "./factories/meepleFactory";
import MeepleCollision from "./views/debug/MeepleCollision";
import MeepleBuilder from "./views/MeepleBuilder";
import spriteUtils from "./utils/spriteUtils";

const speedMultipliers = [1, 2, 6, 60];

function App() {
  const [meeples, setMeeples] = React.useState([]);
  const [play, setPlay] = React.useState(false);
  const [speedMultiplier, setSpeedMultiplier] = React.useState(1);
  const [isDebug, setDebug] = React.useState(config.debug);

  // EVENTS
  const onDebugClick = () => setDebug(!isDebug);
  const onFastForward = () => setSpeedMultiplier(speedMultipliers[(speedMultipliers.findIndex(m => m === speedMultiplier) + 1) % 4]);
  const onPlayStopClick = () => setPlay(oldPlay => !oldPlay);
  const onClearMeeples = () => setMeeples([]);
  const onAddMeeple = (meeple) => {
    const id = meeples.length;
    const characterType = meeples.length % spriteUtils.sprites.length;
    const newMeeple = meeple ? {...meeple, id} : meepleFactory.getNewMeeple({id, characterType});
    setMeeples([...meeples, newMeeple]);
  }

  React.useEffect(() => {
    if (play) {
      const timeout = setTimeout(
        () => setMeeples(meepleUtils.getMeeplesMoved(meeples, config.playground)),
        config.movement.tickEvery / speedMultiplier);
      return () => clearTimeout(timeout);
    }
  }, [play, meeples, speedMultiplier])

  return (
    <>
      <PlayGround  {...config.playground}>
        {meeples.map((meeple) => (
          <React.Fragment key={'fragment_' + meeple.id}>
            {isDebug && <MeepleCollision key={'debug_' + meeple.id} collisionInfo={meeple.collisionInfo} />}
            <Meeple key={'meeple_' + meeple.id} {...meeple} />
          </React.Fragment>)
        )}
      </PlayGround>
      <div > 
        <div>
          <button onClick={() => onAddMeeple()}>➕ Random</button>
          <button onClick={onClearMeeples}>🗑</button>
          <button onClick={onPlayStopClick}>{play ? '◼' : '▶'}</button>
          <button onClick={onFastForward} disabled={!play}>▶▶ x{speedMultiplier}</button>
          <button onClick={onDebugClick}>🪲 {isDebug ? 'On' : 'Off'}</button>
        </div>
      </div>
      <MeepleBuilder onCreate={onAddMeeple}/>
    </>
  );
}

export default App;