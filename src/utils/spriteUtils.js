import config from "../conf/config";
const size = config.meeple.size;
const secondLine = size * 4;

const sprites = [
    {
        image: 1,
        x: 0,
        y: 0
    },
    {
        image: 1,
        x: size * 3,
        y: 0
    },
    {
        image: 1,
        x: size * 6,
        y: 0
    },
    {
        image: 1,
        x: size * 9,
        y: 0
    },
    {
        image: 1,
        x: 0,
        y: secondLine
    },
    {
        image: 1,
        x: size * 3,
        y: secondLine
    },
    {
        image: 1,
        x: size * 6,
        y: secondLine
    },
    {
        image: 1,
        x: size * 9,
        y: secondLine
    },
    {
        image: 2,
        x: 0,
        y: 0
    },
    {
        image: 2,
        x: size * 3,
        y: 0
    },
    {
        image: 2,
        x: size * 6,
        y: 0
    },
    {
        image: 2,
        x: size * 9,
        y: 0
    },
    {
        image: 2,
        x: 0,
        y: secondLine
    },
    {
        image: 2,
        x: size * 3,
        y: secondLine
    },
    {
        image: 2,
        x: size * 6,
        y: secondLine
    },
    {
        image: 2,
        x: size * 9,
        y: secondLine
    },
];

const spriteUtils = {
    sprites
}
export default spriteUtils;