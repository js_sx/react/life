const getNewPosition = (actualPosition, xRange, yRange) => {
    const pathLength = 10;
    let newPosition = { ...actualPosition };

    if (actualPosition.keep) {
        if (actualPosition.keep > 0) {
            newPosition.keep--;
            if (newPosition.x % pathLength === 0) {
                newPosition.y++;
                if (actualPosition.y === yRange[1]) newPosition.keep = 0;
            } else {
                newPosition.x++;
                if (actualPosition.x === xRange[1]) newPosition.keep = 0;
            }
        } else {
            newPosition.keep++;
            if (newPosition.x % pathLength === 0) {
                newPosition.y--;
                if (actualPosition.y === yRange[0]) newPosition.keep = 0;
            } else {
                newPosition.x--;
                if (actualPosition.x === xRange[0]) newPosition.keep = 0;
            }
        }
    } else {
        let validMovement = false;
        while (!validMovement) {
            const isHorizontal = getRandomBool();
            const movement = getRandomIntInclusive(-1, 2);
            const xy = isHorizontal ? 'x' : 'y';
            newPosition = { ...actualPosition, [xy]: actualPosition[xy] + movement };
            newPosition.keep = pathLength * movement;
            validMovement =
                newPosition.x >= xRange[0] &&
                newPosition.x <= xRange[1] &&
                newPosition.y >= yRange[0] &&
                newPosition.y <= yRange[1];
        }
    }
    return newPosition;
}