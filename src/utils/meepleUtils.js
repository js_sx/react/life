import config from "../conf/config";
import randomUtils from "./randomUtils"

const {
    getRandomInt,
    getRandomIntInclusive
} = randomUtils;

const updateMovement = (meeple, movedMeeples) => {
    meeple.position = { ...meeple.position };

    for (const otherMeeple of movedMeeples) {
        if (isCollision(meeple.collisionInfo, otherMeeple.collisionInfo)) {
            meeple.actionCount = 0;
        }
    }

    if (meeple.actionCount) {
        if (meeple.orientation === 'N') meeple.position.y--;
        if (meeple.orientation === 'S') meeple.position.y++;
        if (meeple.orientation === 'E') meeple.position.x++;
        if (meeple.orientation === 'W') meeple.position.x--;
    }
}

const isCollision = (collisionInfo1, collisionInfo2) => {
    const c1End = collisionInfo1.destinationTile;
    const c2Start = collisionInfo2.originTile;
    const c2End = collisionInfo2.destinationTile;

    return (c1End.x === c2Start.x && c1End.y === c2Start.y) ||
        (collisionInfo2.inTransition && c1End.x === c2End.x && c1End.y === c2End.y);
}

const assignCollisionInfo = (meeple) => {
    const isMoving = meeple.actionCount && meeple.action === 'move';
    const inTransition = meeple.position.x % config.meeple.size !== 0 || meeple.position.y % config.meeple.size !== 0


    const xTileIdx = Math.floor(meeple.position.x / config.meeple.size);
    const yTileIdx = Math.floor(meeple.position.y / config.meeple.size);
    let originTile, destinationTile;
    switch (meeple.orientation) {
        case 'N':
            if (inTransition) {
                originTile = { x: xTileIdx, y: yTileIdx + 1 };
                destinationTile = { x: xTileIdx, y: yTileIdx };
            } else {
                originTile = { x: xTileIdx, y: yTileIdx };
                destinationTile = { x: xTileIdx, y: yTileIdx - 1 };
            }
            break;
        case 'W':
            if (inTransition) {
                originTile = { x: xTileIdx + 1, y: yTileIdx };
                destinationTile = { x: xTileIdx, y: yTileIdx };
            } else {
                originTile = { x: xTileIdx, y: yTileIdx };
                destinationTile = { x: xTileIdx - 1, y: yTileIdx };
            }
            break;
        case 'S':
            originTile = { x: xTileIdx, y: yTileIdx };
            destinationTile = { x: xTileIdx, y: yTileIdx + 1 };
            break;
        case 'E':
            originTile = { x: xTileIdx, y: yTileIdx };
            destinationTile = { x: xTileIdx + 1, y: yTileIdx };
            break;
        default:
            console.error('Error occurred assigning collisionInfo to the meeple');
    }
    meeple.collisionInfo = {
        isMoving,
        inTransition,
        originTile,
        destinationTile
    };
}

const assignNewActionCount = (meeple, min, max) => {
    if ( meeple.action === 'move') {
        // Divide by size to calculate tiles to move (and reach edges easier)
        const actionCount = getRandomIntInclusive(min, max / config.meeple.size);
        // Multiply by size to calculate ticks to move
        meeple.actionCount = actionCount * config.meeple.size;
    } else{
        const actionCount = getRandomIntInclusive(min, max);
        meeple.actionCount = actionCount;
    }
}

const assignNewOrientation = (meeple) => {
    const orientations = ['N', 'S', 'E', 'W'].filter(o => o !== meeple.orientation);
    const orientationIdx = getRandomInt(0, orientations.length);
    meeple.orientation = orientations[orientationIdx];
}

const assignNewAction = (meeple) => {
    const actions = ['stay', 'rotate', 'move'].filter(a => a !== meeple.action);
    const actionIdx = getRandomInt(0, actions.length);
    meeple.action = actions[actionIdx];
}

const assignNewImage = (meeple) => {
    switch (meeple.action) {
        case 'move':
            if (meeple.actionCount % 2 === 0) meeple.characterStep++;
            if (meeple.characterStep === 3) meeple.characterStep = 0;
            break;
        case 'stay':
        case 'rotate':
            meeple.characterStep = 1;
            break;
        default:
            console.error('Error occurred assigning image to the meeple');
    }
}

const consumeAction = (meeple) => {
    if (meeple.actionCount) {
        meeple.actionCount--;
        assignNewImage(meeple);
    }
}

const getMeeplesMoved = (meeples, playground) => {
    const newMeeples = [...meeples];
    newMeeples.sort(() => randomUtils.getRandomIntInclusive(-1, 1));
    const movedMeeples = [];
    for (const meeple of newMeeples) {
        movedMeeples.push(getMeepleMoved(meeple, playground, newMeeples.filter(m => m.id !== meeple.id)))
    }
    return movedMeeples;
}

const getMeepleMoved = (meeple, playground, movedMeeples) => {
    const xLimit = playground.width - config.meeple.size;
    const yLimit = playground.height - config.meeple.size;
    const newMeeple = { ...meeple };

    if (!newMeeple.actionCount) assignNewAction(newMeeple);

    switch (newMeeple.action) {
        case 'stay':
            if (!newMeeple.actionCount) assignNewActionCount(newMeeple, 1, config.movement.maxStayTicks);
            break;
        case 'rotate':
            assignNewActionCount(newMeeple, 1, 1);
            assignNewOrientation(newMeeple);
            break;
        case 'move':
            if (!newMeeple.actionCount) {
                if (newMeeple.orientation === 'N') assignNewActionCount(newMeeple, 0, newMeeple.position.y);
                if (newMeeple.orientation === 'S') assignNewActionCount(newMeeple, 0, yLimit - newMeeple.position.y);
                if (newMeeple.orientation === 'E') assignNewActionCount(newMeeple, 0, xLimit - newMeeple.position.x);
                if (newMeeple.orientation === 'W') assignNewActionCount(newMeeple, 0, newMeeple.position.x);
            }
            if (newMeeple.actionCount) {
                assignCollisionInfo(newMeeple);
                updateMovement(newMeeple, movedMeeples);
            }
            break;
        default:
            console.error('Error occurred moving the meeple');
    }
    consumeAction(newMeeple);
    assignCollisionInfo(newMeeple);
    return newMeeple;
}

const meepleUtils = {
    getMeeplesMoved
}

export default meepleUtils;