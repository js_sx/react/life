const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min)) + min;
const getRandomIntInclusive = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
const getRandomBool = () => !!getRandomIntInclusive(0, 1);
const getRandomColor = () => "#" + ((1 << 24) * Math.random() | 0).toString(16);

const exportedFunctions = {
    getRandomInt,
    getRandomIntInclusive,
    getRandomBool,
    getRandomColor
};

export default exportedFunctions;