import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css'

const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate;
renderMethod(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);